Задание:
1) Открыть http://way2automation.com/way2auto_jquery/droppable.php
2) Перетащить элемент в принимающий
3) Убедиться, что текст принимающего элемента изменился

     
Запуск теста:	 
1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
way2.user.userName - логин пользователя
way2.user.password - пароль пользователя

2) mvn clean -Dtest=DragDropCase test