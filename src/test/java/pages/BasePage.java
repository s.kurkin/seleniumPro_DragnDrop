package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void submitElement(WebElement element) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element)).submit();
    }

    public void sendKeysToElement(WebElement element, String keys) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element)).sendKeys(keys);
    }

    public WebElement getElement(WebElement element) {
        return new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void clickElement(WebElement element) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element)).click();
    }
}
