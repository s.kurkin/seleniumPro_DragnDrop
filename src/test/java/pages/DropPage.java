package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DropPage extends BasePage {

    @FindBy(xpath = "//div[@id = 'draggable']")
    private WebElement draggableElement;

    @FindBy(xpath = "//div[@id = 'droppable']")
    private WebElement droppableElement;

    //Frame
    @FindBy(xpath = ".//div[@id='example-1-tab-1']//iframe")
    private WebElement frameDraggable;

    public DropPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getDraggableEl() {
        return getElement(draggableElement);
    }

    public WebElement getDroppableEl() {
        return getElement(droppableElement);
    }

    public WebElement getFrameDraggable() {
        return getElement(frameDraggable);
    }
}
