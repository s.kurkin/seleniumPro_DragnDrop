package cases;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DropPage;
import pages.LoginPage;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;

public class DragDropCase {

    private final String LOGIN_PAGE_URL = "http://way2automation.com/way2auto_jquery/index.php";
    private final String DROP_PAGE_URL = " http://way2automation.com/way2auto_jquery/droppable.php";

    private String userName = "";
    private String userPassword = "";
    private DropPage dropPage;
    private LoginPage loginPage;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws Exception {
        FirefoxDriverManager.getInstance().version("0.19.1").setup();
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testDragDrop() throws IOException {
        logIn();
        dropElement();
        Assert.assertEquals(dropPage.getDroppableEl().getText(), "Dropped!");
    }

    private void dropElement() {
        driver.get(DROP_PAGE_URL);
        dropPage = new DropPage(driver);
        driver.switchTo().frame(dropPage.getFrameDraggable());
        Actions builder = new Actions(driver);
        builder.dragAndDrop(dropPage.getDraggableEl(), dropPage.getDroppableEl())
                .build().perform();
    }

    private void logIn() throws IOException {
        driver.get(LOGIN_PAGE_URL);
        loginPage = new LoginPage(driver);
        loginPage.signInBtnClick();
        loginPage.enterUserName(userName);
        loginPage.enterPassword(userPassword);
        loginPage.submitClick();
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            userName = config.getProperty("way2.user.userName");
            userPassword = config.getProperty("way2.user.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
